import React, { Component } from 'react';
import { Input, Table, Button } from 'antd';
import "antd/dist/antd.css";
 //import {employeeDetails} from './employeesDetailsList';

// let sirish = employeeDetails.filter(record => record.name === 'Sirishnadh'); 
// console.log('sdsdsf',sirish)
//let leavesTAken;
export class employeeDetailsTable extends Component {
    constructor() {
        super()
        this.state = {
            empName: '',
            designation: 'web developer',
            totalDays: 30,
            presentDays: '',
            leavesTaken: '',
            approvedLeaves:2,
            lossOfPay:'',
            basicSalary: "",
            totalSalary: '',
            selectedData: []
        }
    }
    handleChange=(event)=>{
        this.setState({ [event.target.name]: event.target.value });
    }
    handleClick=(index)=> {
        const employeeRecord = [...this.state.selectedData] 
        let leavesTaken= this.state.totalDays-this.state.presentDays;
        let salaryPerDay=this.state.basicSalary/30;
        employeeRecord.push({
            key: index,
            empName: this.state.empName,
            designation: 'web developer',
            totalDays:  30 ,
            presentDays: this.state.presentDays,
            leavesTaken: leavesTaken,
            approvedLeaves: leavesTaken>2 ? 2 :leavesTaken ,
            lossOfPay:leavesTaken>2 ? leavesTaken-this.state.approvedLeaves: 0,
            basicSalary: parseFloat(this.state.basicSalary),
            totalSalary:parseInt( leavesTaken>2 ? (this.state.totalDays-(leavesTaken-2)) * salaryPerDay : this.state.basicSalary),
        })
        
        ;
      
        console.log("leavesTaken", this.state.leavesTaken);
        this.setState({
          selectedData: employeeRecord,

        });
     
        // const sirish = employeeDetails.filter(record => record.name === 'Manoj');
        // if(sirish.name === employeeRecord.empName){
        //     this.setState({
        //         selectedData:employeeRecord,
        //     });

        // }else{
        //     console.log('name',sirish.name);
        //     alert('Record not found');
            
        // }
        // console.log('name',sirish);
        // console.log("employeeRecord", employeeRecord);
      }
      
      
    render() {
        const columns = [
            {
                title: 'Name',
                dataIndex: 'empName',
                render: text => <a href = 'www.google.com'>{text}</a>,
            },
            {
                title: 'Designation',
                dataIndex: 'designation',
            },
            {
                title: 'Total Days',
                dataIndex: 'totalDays',
            },
            {
                title: 'Present Days',
                dataIndex: 'presentDays',
            },
            {
                title: 'Leaves Taken',
                dataIndex: 'leavesTaken',
            },
            {
                title:"Appruved Leaves",
                dataIndex:"approvedLeaves"
            },
            {
              title:"Loss Of Pay",
              dataIndex:"lossOfPay"
            },
            {
                title: 'Basic salary',
                dataIndex: 'basicSalary',
            },
            {
                title: 'Total Salary',
                dataIndex: 'totalSalary',
            },
        
          ];
          
        return (
            <div>
                <div style = {{marginTop:50,marginLeft: 10}}>
                    <div style ={{display:"flex"}}>
                   <label> Name: </label><Input style = {{width:'15%',margin:'0px 20px'}} type = 'text' name = "empName" placeholder="Enter employee name" onChange={this.handleChange}/><br /><br />
                   <label>  present Days: </label><Input style = {{width:'15%',margin:'0px 20px'}} type = 'number' name = "presentDays" placeholder="Enter the value" onChange={this.handleChange}/><br /><br />
                 <label>   Basic Salary:</label> <Input style = {{width:'10%',margin:'0px 20px'}} type = 'number' name = "basicSalary" placeholder="Enter the value" onChange={this.handleChange}/><br /><br />
                   </div>
                    <Button onClick = {this.handleClick} type = 'primary' style={{possiton:"absolute",left:"90%"}}>Submit</Button><br /><br />
                    <Table columns={columns} dataSource = {this.state.selectedData} rowKey={(index)=> index+1} pagination = {false}/>
                </div>
            </div>
        )
    }
}

export default employeeDetailsTable

