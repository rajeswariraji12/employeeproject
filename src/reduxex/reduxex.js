import React from 'react';
import {createStore} from 'redux';
// import Redux from 'redux-saga';
// import ReactRedux from 'redux-saga';
import {Provider,connect} from 'react-redux';



const connects = connect;
const Providers = Provider;
const createStores = createStore;

// Reducer
const initialState = {
  url: '',
  loading: false,
  error: false,
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUESTED_DOG':
      return {
        url: '',
        loading: true,
        error: false,
      };
    case 'REQUESTED_DOG_SUCCEEDED':
      return {
        url: action.url,
        loading: false,
        error: false,
      };
    case 'REQUESTED_DOG_FAILED':
      return {
        url: '',
        loading: false,
        error: true,
      };
    default:
      return state;
  }
};

// Action Creators
const requestDog = () => {
  return { type: 'REQUESTED_DOG' }
};

const requestDogSuccess = (data) => {
  return { type: 'REQUESTED_DOG_SUCCEEDED', url: data.message }
};

const requestDogError = () => {
  return { type: 'REQUESTED_DOG_FAILED' }
};

const fetchDog = (dispatch) => {
  dispatch(requestDog());
  return fetch('http://demo9492836.mockable.io/getResult')
    .then(res => res.json())
    .then(
      data => dispatch(requestDogSuccess(data)),
      err => dispatch(requestDogError(err))
    );
};

// Component
class App extends React.Component {
  render () {
    return (
      <div>
        <button onClick={() => fetchDog(this.props.dispatch)}>Show Dog</button>
          {this.props.loading 
            ? <p>Loading...</p> 
            : this.props.error
                ? <p>Error, try again</p>
                : <p><img src={this.props.url} alt="dog"/></p>}
      </div>
    )
  }
}

// Store
const store = createStores(reducer);

const ConnectedApp = connects((state) => {
       return state
})(App);

// Container component
class reduxExample extends React.Component{
    render(){
        return(
            <div>
        <Providers store={store}>
          <ConnectedApp />
        </Providers>,
        </div>
      )
    }

}
export default reduxExample